# Metodología de Desarrollo de Software Libre (MDSL) Versión 2.0 hecha en CENDITEL

Sitio web creado para mostrar la MDSL y que sirva como guía para la construcción de un software.

Permite descargar la metodología en [Formato PDF](https://argenisosorio.github.io/mdsl-cenditel/docs/mpdcsl.pdf)

[Visitar sitio](https://argenisosorio.github.io/mdsl-cenditel/)


